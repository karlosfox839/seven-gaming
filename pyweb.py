from pywebio import start_server
from pywebio.input import *
from pywebio.output import *
import webbrowser as wb
from pywebio.session import run_async, run_js


import asyncio

chat_msgs = []

online_users = set()

MAX_MESSAGES_COUNT = 100

async def main():
	global chat_msgs

	put_markdown("## 📢 Welcome To Online Chat\n This Chat have a new function if have 100 messages automatically deleting !!")


	msg_box = output()
	put_scrollable(msg_box, height=300, keep_bottom=True)

	nickname = await input("Login for Chat", required=True, placeholder="Ваше ник..", validate=lambda n: "Nickname Have !" if n in online_users or n == "📢" else None)

	online_users.add(nickname)

	chat_msgs.append(("📢", f"'{nickname}' Connected This Chat"))
	msg_box.append(put_markdown(f"'{nickname}' Connected This Chat"))

	refresh_task = run_async(refresh_msg(nickname, msg_box))

	while True:
		data = await input_group("🌐 New Message", [
			input(placeholder="Text Message", name="msg"),
			actions(name="cmd", buttons=["Send", {'label':"Log Out ", 'type':'cancel'}])
			], validate=lambda m: ('msg', "Message for sending...") if m["cmd"] == "Send" and not m["msg"] else None)

		if data is None:
			break

		msg_box.append(put_markdown(f"'{nickname}' : {data['msg']}"))
		chat_msgs.append((nickname, data['msg']))
		if data == "Fuck":
			chat_msgs.append(("📢", f" No Saying Fuck Please"))
			msg_box.append(put_markdown(f"No Saying Fuck Please"))

	# exit chat 

	refresh_task.close()

	online_users.remove(nickname)
	toast("Вы вышли чата !")
	msg_box.append(put_markdown(f" 📢 User '{nickname}' Log Out This Chat"))
	chat_msgs.append(('📢', f"User'{nickname}' Log Out This Chat"))

	put_buttons(["Reload"], onclick=lambda btn: run_js('window.location.reload'))



async def refresh_msg(nickname, msg_box):

	global chat_msgs
	last_idx = len(chat_msgs)

	while True:
		await asyncio.sleep(1)

		for m in chat_msgs[last_idx:]:
			if m[0] != nickname:
				msg_box.append(put_markdown(f"'{m[0]}' : {m[1]}"))

		if len(chat_msgs) > MAX_MESSAGES_COUNT:
			chat_msgs = chat_msgs[len(chat_msgs) // 2:]

		last_idx = len(chat_msgs)

if __name__ == "__main__":

	start_server(main, debug=True, port=8080, cdn=False)
	wb.open(main)

